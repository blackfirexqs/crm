<?php  
namespace app\models\form;
use Yii;

use yii\base\Model; 

class CustomReportForm extends Model
{
    public $date_from;
    public $date_to;


    public function rules()
    {
        return [
            [['date_from','date_to'],'safe'],
            
        ];
    }


    public function attributeLabels()
    {
        return [
            'date_from' => 'Начиная с',
            'date_to' => 'До'
        ];
    }
}