<?php

namespace app\models;

use yii\web\IdentityInterface;
use yii\db\ActiveRecord;


use Yii;

/**
 * This is the model class for table "f_user".
 *
 * @property int $user_id
 * @property string $login
 * @property string $password
 * @property string $name
 * @property int $status
 * @property int $role
 */
class User extends ActiveRecord implements IdentityInterface
{
    
  
    public $username;
    public $rememberMe = true;

    public static function tableName()
    {
        return 'f_user';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'name', 'status', 'role'], 'required'],
            [['status', 'role'], 'integer'],
            [['login', 'password'], 'string', 'max' => 512],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Идентификатор пользователя',
            'login' => 'Логин',
            'password' => 'Пароль',
            'name' => 'Имя',
            'status' => 'Статус',
            'role' => 'Роль',
        ];
    }
    
     public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        //return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {

        return $this->user_id;
    }

    public function getAuthKey()
    {
        //return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        //return $this->authKey === $authKey;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
