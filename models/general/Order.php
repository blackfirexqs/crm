<?php

namespace app\models\general;

use Yii;

/**
 * This is the model class for table "f_order".
 *
 * @property int $order_id
 * @property string $order_general_id
 * @property string $order_local_id
 * @property int $customer_id
 * @property int $order_type_id
 * @property string $date_in
 * @property string $date_out
 * @property int $total
 * @property int $prepayment
 * @property int $postpayment
 * @property string $comment
 * @property int $product_type_id
 * @property int $facade_top_main_id
 * @property int $facade_top_extra_id
 * @property int $facade_bottom_main_id
 * @property int $facade_bottom_extra_id
 * @property int $custom
 * @property int $radius
 * @property int $frame_color_id
 * @property int $glass_color_id
 * @property int $mirror_color_id
 * @property int $table_top_color_id
 * @property int $wall_panel_color_id
 * @property int $kant_color_id
 * @property int $base_color_id
 * @property int $dry_material_type_id
 * @property int $dry_size
 * @property int $wash_material_type_id
 * @property int $wash_size
 * @property int $tray_size
 * @property int $handle_id
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
               [['order_general_id'], 'required'],
               [['order_local_id','customer_id','order_type_id','date_in','date_out','date_out_real','total','prepayment','postpayment','comment','product_type_id','facade_top_main_id','facade_top_extra_id','facade_top_color_id','facade_top_custom','facade_top_date', 'facade_bottom_main_id','facade_bottom_extra_id','facade_bottom_color_id','facade_bottom_custom','facade_bottom_date','custom','radius','frame_color_id','glass_color_id','mirror_color_id','table_top_color_id','table_top_size','wall_panel_color_id','kant_color_id','base_color_id','dry_type_id','dry_size','wash_type_id','wash_type_id','tray_size','handle_id','status'], 'safe'], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Порядковый номер',
            'order_general_id' => 'Клиентский номер заказа',
            'order_local_id' => 'Внутренний номер заказа',
            'customer_id' => 'Заказачик',
            'order_type_id' => 'Тип заказа',
            'date_in' => 'Дата заказа',
            'date_out' => 'Планируемая дата отгрузки',
            'date_out_real' => 'Фактическая дата отгрузки',
            'total' => 'Сумма заказа',
            'prepayment' => 'Предоплата',
            'postpayment' => 'Постоплата',
            'comment' => 'Примечание',
            'product_type_id' => 'Тип изделия',
            'facade_top_main_id' => 'Тип фасада (верх, основной)',
            'facade_top_color_id'=> 'Цвет фасада (верх)',
            'facade_top_extra_id' => 'Тип фасада (верх, дополнительный)',
            'facade_top_custom' => 'Заказная позиция (верх)',
            'facade_top_date' => 'Дата поставки (верх)',
            'facade_bottom_main_id' => 'Тип фасада (низ, основной)',
            'facade_bottom_extra_id' => 'Тип фасада (низ, дополнительный)',
            'facade_bottom_color_id'=> 'Цвет фасада (низ)',
            'facade_bottom_custom' => 'Заказная позиция (низ)',
            'facade_bottom_date' => 'Дата поставки (низ)',
            'custom' => 'Заказная позиция',
            'radius' => 'Радиус',
            'frame_color_id' => 'Цвет каркаса',
            'glass_color_id' => 'Цвет стекла',
            'mirror_color_id' => 'Цвет зеркала',
            'table_top_color_id' => 'Цвет столешницы',
            'table_top_size' => 'Размер столешницы, мм',
            'wall_panel_color_id' => 'Цвет cтеновой панели',
            'kant_color_id' => 'Цвет канта',
            'base_color_id' => 'Цвет цоколя',
            'dry_type_id' => 'Тип материала сушки',
            'dry_size' => 'Размер сушки',
            'wash_type_id' => 'Тип мойки',
            'tray_size' => 'Размер лотка, мм',
            'handle_type_id' => 'Ручки',
            'status' => 'Статус',
        ];
    }
}
