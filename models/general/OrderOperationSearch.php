<?php

namespace app\models\general;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\general\OrderOperation;

/**
 * OrderOperationSearch represents the model behind the search form of `app\models\general\OrderOperation`.
 */
class OrderOperationSearch extends OrderOperation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_operation_id', 'material_type_id', 'operation_id', 'order_id', 'cost', 'user_id'], 'integer'],
            [['date', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderOperation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_operation_id' => $this->order_operation_id,
            'material_type_id' => $this->material_type_id,
            'operation_id' => $this->operation_id,
            'order_id' => $this->order_id,
            'cost' => $this->cost,
            'user_id' => $this->user_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
