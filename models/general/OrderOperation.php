<?php

namespace app\models\general;

use Yii;

/**
 * This is the model class for table "f_order_operation".
 *
 * @property int $order_operation_id
 * @property int $material_type_id
 * @property int $operation_id
 * @property int $order_id
 * @property int $cost
 * @property int $user_id
 * @property string $date
 * @property string $description
 */
class OrderOperation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_order_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_operation_id' => 'Порядковый номер',
            'material_type_id' => 'Тип материала',
            'operation_id' => 'Операция',
            'order_id' => 'Заказ',
            'cost' => 'Ставка',
            'user_id' => 'Первый исполнитель',
            'user_id_2' => 'Второй исполнитель',
            'date' => 'Дата',
            'description' => 'Описание',
        ];
    }
}
