<?php

namespace app\models\general;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\general\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\general\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'customer_id', 'order_type_id', 'total', 'prepayment', 'postpayment', 'product_type_id', 'facade_top_main_id', 'facade_top_extra_id', 'facade_bottom_main_id', 'facade_bottom_extra_id', 'custom', 'radius', 'frame_color_id', 'glass_color_id', 'mirror_color_id', 'table_top_color_id', 'wall_panel_color_id', 'kant_color_id', 'base_color_id', 'dry_type_id', 'dry_size', 'wash_type_id',  'tray_size', 'handle_type_id','status'], 'integer'],
            [['order_general_id', 'order_local_id', 'date_in', 'date_out', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$role)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'customer_id' => $this->customer_id,
            'order_type_id' => $this->order_type_id,
            'date_in' => $this->date_in,
            'date_out' => $this->date_out,
            'total' => $this->total,
            'prepayment' => $this->prepayment,
            'postpayment' => $this->postpayment,
            'product_type_id' => $this->product_type_id,
            'facade_top_main_id' => $this->facade_top_main_id,
            'facade_top_extra_id' => $this->facade_top_extra_id,
            'facade_bottom_main_id' => $this->facade_bottom_main_id,
            'facade_bottom_extra_id' => $this->facade_bottom_extra_id,
            'custom' => $this->custom,
            'radius' => $this->radius,
            'frame_color_id' => $this->frame_color_id,
            'glass_color_id' => $this->glass_color_id,
            'mirror_color_id' => $this->mirror_color_id,
            'table_top_color_id' => $this->table_top_color_id,
            'wall_panel_color_id' => $this->wall_panel_color_id,
            'kant_color_id' => $this->kant_color_id,
            'base_color_id' => $this->base_color_id,
            'dry_type_id' => $this->dry_type_id,
            'dry_size' => $this->dry_size,
            'wash_type_id' => $this->wash_type_id,
            'tray_size' => $this->tray_size,
            'handle_type_id' => $this->handle_type_id,
            'status' => $this->status,
        
        ]);

        if ($role != 1)
        {
            $query->andFilterWhere([
                'user_id' => Yii::$app->user->getId(),
            ]);
        }

        $query->andFilterWhere(['like', 'order_general_id', $this->order_general_id])
            ->andFilterWhere(['like', 'order_local_id', $this->order_local_id])
            ->andFilterWhere(['like', 'comment', $this->comment])->orderBy(['status' => SORT_ASC]);

        return $dataProvider;
    }
}
