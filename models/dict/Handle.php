<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_handle".
 *
 * @property int $handle_id
 * @property string $name
 */
class Handle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_handle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'handle_id' => 'Номер ручки',
            'name' => 'Наименование',
        ];
    }
}
