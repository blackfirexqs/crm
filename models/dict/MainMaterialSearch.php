<?php

namespace app\models\dict;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\dict\MainMaterial;

/**
 * MainMaterialSearch represents the model behind the search form of `app\models\dict\MainMaterial`.
 */
class MainMaterialSearch extends MainMaterial
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_material_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainMaterial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'main_material_id' => $this->main_material_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
