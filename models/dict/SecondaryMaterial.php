<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_secondary_material".
 *
 * @property int $secondary_material_id
 * @property int $name
 */
class SecondaryMaterial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_secondary_material';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'secondary_material_id' => 'Secondary Material ID',
            'name' => 'Name',
        ];
    }
}
