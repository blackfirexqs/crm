<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_operation".
 *
 * @property int $operation_id
 * @property string $name
 * @property double $value
 */
class Operation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
            [['value'], 'number'],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id' => 'Номер операции',
            'name' => 'Наименование',
            'value' => 'Значение',
        ];
    }
}
