<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_main_material".
 *
 * @property int $main_material_id
 * @property string $name
 */
class MainMaterial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_main_material';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'main_material_id' => 'Номер основного материала',
            'name' => 'Наименование',
        ];
    }
}
