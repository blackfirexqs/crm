<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_color_to_material_type".
 *
 * @property int $color_id
 * @property int $material_type_id
 */
class ColorToMaterialType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_color_to_material_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color_id', 'material_type_id'], 'required'],
            [['color_id', 'material_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'material_type_id' => 'Material Type ID',
        ];
    }
}
