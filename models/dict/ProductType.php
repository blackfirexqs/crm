<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_product_type".
 *
 * @property int $product_type_id
 * @property string $name
 */
class ProductType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type_id' => 'Номер типа изделия',
            'name' => 'Наименование',
        ];
    }
}
