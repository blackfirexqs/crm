<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_customer".
 *
 * @property int $customer_id
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 * @property string $telephone
 * @property string $email
 * @property string $description
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'patronymic', 'telephone', 'email', 'description'], 'required'],
            [['firstname'], 'string', 'max' => 256],
            [['lastname', 'patronymic', 'telephone', 'email', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Номер заказчика',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'telephone' => 'Телефон',
            'email' => 'Почта',
            'description' => 'Описание',
        ];
    }
}
