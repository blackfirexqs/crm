<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_material_type".
 *
 * @property int $material_type_id
 * @property string $name
 */
class MaterialType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_material_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'material_type_id' => 'Номер типа материала',
            'name' => 'Наименование',
        ];
    }
}
