<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_user".
 *
 * @property int $user_id
 * @property string $login
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 * @property int $status
 * @property int $role
 * @property string $position
 * @property string $auth_key
 * @property string $access_token
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'firstname', 'lastname', 'patronymic', 'status', 'role'], 'required'],
            [['status', 'role'], 'integer'],
            [['login', 'password'], 'string', 'max' => 512],
            [['firstname', 'lastname', 'patronymic', 'position'], 'string', 'max' => 256],
            [['auth_key', 'access_token'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'login' => 'Login',
            'password' => 'Password',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'patronymic' => 'Patronymic',
            'status' => 'Status',
            'role' => 'Role',
            'position' => 'Position',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }
}
