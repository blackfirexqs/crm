<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_order_type".
 *
 * @property int $order_type_id
 * @property string $name
 */
class OrderType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_order_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_type_id' => 'Номер типа заказа',
            'name' => 'Наименование',
        ];
    }
}
