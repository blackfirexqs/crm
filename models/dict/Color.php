<?php

namespace app\models\dict;

use Yii;

/**
 * This is the model class for table "f_color".
 *
 * @property int $color_id
 * @property string $name
 */
class Color extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Номер цвета',
            'name' => 'Название цвета',
        ];
    }
}
