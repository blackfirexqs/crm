<?php

namespace app\controllers;

use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\filters\AccessControl;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\form\UpdateDictForm;
use app\models\general\Order;
use app\models\general\OrderSearch;
use app\models\dict\MaterialType;
use app\models\dict\Operation;
use app\models\dict\Color;
use app\models\dict\SecondaryMaterial;
use app\models\User;
use app\models\general\OrderOperation;
use app\models\general\OrderOperationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use DateTime;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                
                'rules' => [
                    [
                    'actions' => ['index','view','update','create','delete'],
                    'allow' => true,
                    'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $update_form = new UpdateDictForm();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->role);


        $directory = realpath(dirname(__FILE__).'/../files/orders/');
        $scanned_directory = array_slice(scandir($directory), 2);

        if ($update_form->load(Yii::$app->request->post()) && $update_form->validate()) 
        {
            foreach ($scanned_directory as  $kkk=> $sd) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($directory.'/'.$sd);
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();
                // print_r('<pre>');
                // print_r($rows);
                // print_r('</pre>');
                // die;
                $order_general_id = $rows[3][1];

                $order_gl_ids = explode(' ', $order_general_id);
                $order_local_id_value = '';
                $order_general_id_value = '';
                foreach ($order_gl_ids as $key => $order_gl_id) {
                    if ($key == 0) {
                        $order_local_id_value = $order_gl_id;
                    } else {
                        $order_general_id_value .= $order_gl_id . ' ';
                    }
                }
                $order_general_id_value = substr($order_general_id_value, 0, -1);
                //проверяем если в базе такой номер заказа
                $check_order = Order::find()->where(['order_local_id' => $order_local_id_value])->one();

                //если заказа нет, то добавляем
                if(!$check_order)
                {

                    $order = new Order();

                    $order->order_general_id = $order_general_id_value;
                    $order->order_local_id = $order_local_id_value;
                    

                    $date = new DateTime();
                    $date->modify('+1 hour');
                    $order->date_in = $date->format('Y-m-d');
                    $date_out = $date->modify('+21 day');
                    $order->date_out = $date_out->format('Y-m-d');
                    //
                   
                    $order->user_id = Yii::$app->user->getId();
                    $order->save();
                    
                    $check_order = Order::find()->where(['order_local_id' => $order_local_id_value])->asArray()->one();

                }

                
                $order_id = $check_order['order_id'];
                
              
                $status = false;

                $last_row = end($rows);
                foreach ($rows as $key => $row) {

                    if($key > 10)
                    {

                        if($status)
                        {   
                            
                            $check_order_operation = OrderOperation::find()->where(['material_type_id' => $order_operation->material_type_id])->andWhere(['operation_id' => $order_operation->operation_id])->andWhere(['order_id' => $order_operation->order_id])->one();

                            if(!$check_order_operation)
                            {
                                

                                $order_operation->save();


                            } else
                            {
                                
                                $check_order_operation->cost = $order_operation->cost;
                                $check_order_operation->save();
                            }
                            $status = false;
                        }

                        //если нашли тип материала
                        if($row[0] && !$row[1])
                        {
                            $material_type = MaterialType::find()->where(['name' =>$row[0]])->asArray()->one();
                            if ($material_type)
                            {
                                $material_type = $material_type['material_type_id'];
                                

                            } else {}
                        }

                        //если нашли операцию
                        if($row[1])
                        {
                            $operation = Operation::find()->where(['name'=>$row[0]])->asArray()->one();
                            if($operation)
                            {
                                $order_operation = new OrderOperation();

                                $order_operation->material_type_id = $material_type;
                                $order_operation->order_id = $order_id;
                                $order_operation->operation_id = $operation['operation_id'];
                                $order_operation->cost = round($operation['value']*$row[2],2);
                                $status = true;

                            }
 
                        }
                        if($last_row[0] == $row[0] && $last_row[1] == $row[1] && $last_row[2] == $row[2])
                        {
                             $check_order_operation = OrderOperation::find()->where(['material_type_id' => $order_operation->material_type_id])->andWhere(['operation_id' => $order_operation->operation_id])->andWhere(['order_id' => $order_operation->order_id])->one();

                            if(!$check_order_operation)
                            {
                                $order_operation->save();

                            } else
                            {
                                $check_order_operation->cost = $order_operation->cost;
                                $check_order_operation->save();
                            }
                            $status = false;
                        }


                    }

                }
               


                
            }


        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'update_form' => $update_form,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpd()
    {
        $orders = Order::find()->all();
        foreach ($orders as $order) {
            $order_general_id = $order->order_general_id;

            $order_gl_ids = explode(' ', $order_general_id);
            $order_local_id_value = '';
            $order_general_id_value = '';
            foreach ($order_gl_ids as $key => $order_gl_id) {
                if ($key == 0) {
                    $order_local_id_value = $order_gl_id;
                } else {
                    $order_general_id_value .= $order_gl_id . ' ';
                }
            }
            $order_general_id_value = substr($order_general_id_value, 0, -1);


            $order->order_general_id = $order_general_id_value;
            $order->order_local_id = $order_local_id_value;
            $order->save();
        }
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $final_order_operations = array();
        $order_operations = OrderOperation::find()->where(['order_id' => $id])->all();
        foreach ($order_operations as $order_operation) {
            $material_type = MaterialType::find()->where(['material_type_id' => $order_operation->material_type_id])->one();
            if ($material_type) { 
                $material_type_name = $material_type->name;
            } else {
                $material_type_name = '';
            }
            $operation = Operation::find()->where(['operation_id' => $order_operation->operation_id])->one();
            $user_data = User::find()->where(['user_id' => $order_operation->user_id])->one();
            if ($user_data) {
                $user_data_name = $user_data->lastname . ' ' . $user_data->firstname . ' ' . $user_data->patronymic;
            } else {
                $user_data_name = '';
            }
            $user_2_data = User::find()->where(['user_id' => $order_operation->user_id_2])->one();
            if ($user_2_data) {
                $user_2_data_name = $user_2_data->lastname . ' ' . $user_2_data->firstname . ' ' . $user_2_data->patronymic ;
            } else {
                $user_2_data_name = '';
            }
            $final_order_operations[$order_operation->material_type_id][] = array(
                'order_operation_id' => $order_operation->order_operation_id,
                'material_type_id' => $order_operation->material_type_id,
                'material_type_name' => $material_type_name,
                'operation_name' => $operation->name,
                'user_name' => $user_data_name,
                'user_2_name' =>  $user_2_data_name,
                'cost' => $order_operation->cost,
            );
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'order_operations' => $final_order_operations
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) ) {

        	$model->user_id = Yii::$app->user->getId();
        	if($model->save())
        	{
            	return $this->redirect(['view', 'id' => $model->order_id]);
        	}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);



        if ($model->load(Yii::$app->request->post())) {
            $facade_top_extra_id = SecondaryMaterial::find()->where(['name' => $model->facade_top_extra_id])->asArray()->one();
            $model->facade_top_extra_id = $facade_top_extra_id['secondary_material_id'];

            $facade_top_color_id = Color::find()->where(['name' => $model->facade_top_color_id])->asArray()->one();
            $model->facade_top_color_id = $facade_top_color_id['color_id'];

            $facade_bottom_extra_id = SecondaryMaterial::find()->where(['name' => $model->facade_bottom_extra_id])->asArray()->one();
            $model->facade_bottom_extra_id = $facade_bottom_extra_id['secondary_material_id'];

            $facade_bottom_color_id = Color::find()->where(['name' => $model->facade_bottom_color_id])->asArray()->one();
            $model->facade_bottom_color_id = $facade_bottom_color_id['color_id'];

            $model->save();

            return $this->redirect(['view', 'id' => $model->order_id]);
        }

        $facade_top_extra_id = SecondaryMaterial::find()->where(['secondary_material_id' => $model->facade_top_extra_id])->asArray()->one();
        $model->facade_top_extra_id = $facade_top_extra_id['name'];

        $facade_bottom_extra_id = SecondaryMaterial::find()->where(['secondary_material_id' => $model->facade_bottom_extra_id])->asArray()->one();
        $model->facade_bottom_extra_id = $facade_bottom_extra_id['name'];

        $facade_top_color_id = Color::find()->where(['color_id' => $model->facade_top_color_id])->asArray()->one();
        $model->facade_top_color_id = $facade_top_color_id['name'];

        $facade_bottom_color_id = Color::find()->where(['color_id' => $model->facade_bottom_color_id])->asArray()->one();
        $model->facade_bottom_color_id = $facade_bottom_color_id['name'];

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        OrderOperation::deleteAll(['order_id' => $id]);

   
        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
