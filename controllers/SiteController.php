<?php

namespace app\controllers;

use Yii;

use yii\filters\AccessControl;

use yii\web\Controller;

use yii\web\Response;

use yii\filters\VerbFilter;

use yii\db\ActiveRecord; 

use app\models\LoginForm;

use app\models\User;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                
                'rules' => [
                    [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                    ],
                    [
                    'actions' => ['index', 'logout','contact','dictionary'],
                    'allow' => true,
                    'roles' => ['@'],
                    ],
                    [
                    'actions' => ['workers', 'gii','dictionary','operation','about'],
                    'allow' => true,
                    'roles' => ['canAdmin'],
                    ],

                   
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [

            'error' => [

                'class' => 'yii\web\ErrorAction',

            ],

            'captcha' => [

                'class' => 'yii\captcha\CaptchaAction',

                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,

            ],

        ];
    }

    
    public function actionIndex()

    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) 
        {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) 
        {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [

        'model' => $model,

        ]);

    }


    public function actionLogout()

    {

        Yii::$app->user->logout();
        return $this->goHome();

    }

    public function actionDictionary()
    {
        return $this->render('dictionary');
    }
}
