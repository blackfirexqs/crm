<?php

namespace app\controllers;

use Yii;
use app\models\dict\MaterialType;
use app\models\dict\MaterialTypeSearch;
use app\models\form\UpdateDictForm;
use app\models\form\DeleteDictForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DictMaterialTypeController implements the CRUD actions for MaterialType model.
 */
class DictMaterialTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaterialType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialTypeSearch();
        $update_form = new UpdateDictForm();
        $delete_form = new DeleteDictForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

       // ---------------- ДОБАВЛЕНИЕ ЭЛЕМЕНТОВ СПРАВОЧНИКА ИЗ ФАЙЛА ---------------
        if ($update_form->load(Yii::$app->request->post()) && $update_form->validate()) 
        {
            
            $importing_items = [];
            $existing_items = [];
            $items_to_add = [];

            $file_path = realpath(dirname(__FILE__).'/../files/dict/material-type.xlsx');
            if (file_exists($file_path)) 
            {

                $items = MaterialType::find()->asArray()->all();
                foreach ($items as $item) 
                {
                    array_push($existing_items, $item['name']);
                }
                
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($file_path);
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();
                foreach ($rows as $row) 
                {
                    array_push($importing_items, $row[0]);
                }
                
                $items_to_add = array_diff($importing_items,$existing_items);
                    
                foreach ($items_to_add as $item) 
                {
                    $import = new MaterialType();
                    $import->name = $item;
                    $import->save();
                    
                }
              
            } 
  
        }

        // ---------------- УДАЛЕНИЕ ВСЕХ ЭЛЕМЕНТОВ СПРАВОЧНИКА ---------------
        if ($delete_form->load(Yii::$app->request->post()) && $delete_form->validate()) 
        {
            $data = MaterialType::deleteAll();
            
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'update_form' => $update_form,
            'delete_form' => $delete_form,

        ]);
    }

    /**
     * Displays a single MaterialType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MaterialType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MaterialType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->material_type_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MaterialType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->material_type_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MaterialType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MaterialType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaterialType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaterialType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
