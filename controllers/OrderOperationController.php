<?php

namespace app\controllers;

use Yii;
use DateTime;
use app\models\general\OrderOperation;
use app\models\general\OrderOperationSearch;
use app\models\general\Order;
use app\models\dict\Operation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderOperationController implements the CRUD actions for OrderOperation model.
 */
class OrderOperationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderOperation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderOperationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderOperation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrderOperation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderOperation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_operation_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderOperation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $data = OrderOperation::find()->where(['order_operation_id' => $model->order_operation_id])->one();
            $post = Yii::$app->request->post();
       


            if($post['OrderOperation']['user_id'])
            {
                $data->user_id = $post['OrderOperation']['user_id'];
            }
            if($post['OrderOperation']['user_id_2'])
            {
                $data->user_id_2 = $post['OrderOperation']['user_id_2'];
            }


            if($model->date == '0000-00-00 00:00:00' || ( $post['OrderOperation']['user_id'] || $post['OrderOperation']['user_id_2'] ) )
            {
                $curent_date = new DateTime();
                $curent_date->modify('+1 hour');
                if($model->date != '0000-00-00 00:00:00')
                {
                    $data->date = $post['OrderOperation']['date'];
                } else
                {
                    $data->date = $curent_date->format('Y-m-d H:i:s'); 
                }
                

            } else
            {
                $data->user_id = 0;
                $data->user_id_2 = 0;
                $data->date = '0000-00-00 00:00:00';
            }

            $data->save();

            $order_operations_all = OrderOperation::find()->where(['order_id' => $model->order_id])->asArray()->all();
            $order_operations_complete = OrderOperation::find()->where(['order_id' => $model->order_id])->andWhere(['not', ['user_id' => '0']])->asArray()->all();

            $order_status = Order::find()->where(['order_id' => $model->order_id])->one();


            $loading_operations = Operation::find()->where(['like','name','Погрузочные работы'])->asArray()->all();
           	$loading_operations_id = '';

            foreach ($loading_operations as $lo) {
            	$loading_operations_id .= $lo['operation_id'].',';      	
            }
            $loading_operations_id = substr($loading_operations_id, 0,-1);
           


            $order_not_loading_operation_all = OrderOperation::find()->where(['order_id' => $model->order_id])->andWhere(['not in','operation_id',$loading_operations_id])->asArray()->all();
            $order_not_loading_operation_complete = OrderOperation::find()->where(['order_id' => $model->order_id])->andWhere(['not in','operation_id',$loading_operations_id])->andWhere(['not', ['user_id' => '0']])->asArray()->all();

            

           


            if( count($order_not_loading_operation_all) ==  count($order_not_loading_operation_complete))
            {
            	$order_status->status = '1';
            } else {

            	$order_status->status = '0';
            }


            if ( count($order_operations_all) == count($order_operations_complete) )
            {
            	$order_status->status = '2';
            }


            $order_status->save();


            
            
            return $this->redirect(['order/'.$model->order_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderOperation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderOperation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderOperation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderOperation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
