<?php

namespace app\controllers;

use Yii;
use app\models\dict\Customer;
use app\models\dict\CustomerSearch;
use app\models\form\UpdateDictForm;
use app\models\form\DeleteDictForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DictCustomerController implements the CRUD actions for Customer model.
 */
class DictCustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $update_form = new UpdateDictForm();
        $delete_form = new DeleteDictForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // if ($update_form->load(Yii::$app->request->post()) && $update_form->validate()) 
        // {
            
        //     $importing_items = [];
            
        //     $items_to_add = [];


        //     $file_path = realpath(dirname(__FILE__).'/../files/dict/customer.xlsx');
        //     if (file_exists($file_path)) 
        //     {

        //         $items = Customer::find()->asArray()->all();
                
        //         foreach ($items as $item) 
        //         {
        //             $existing_items[] = array(
        //                 'firstname' => $item['firstname'],
        //                 'lastname' => $item['lastname'],
        //                 'patronymic' => $item['patronymic'],
        //                 'telephone' => $item['telephone'],
        //                 'email' => $item['email']

        //             );
                    
        //         }


        //         print_r('<pre>');
        //         print_r($existing_items);
        //         print_r('</pre>');

                
        //         $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        //         $reader->setReadDataOnly(true);
        //         $spreadsheet = $reader->load($file_path);
        //         $worksheet = $spreadsheet->getActiveSheet();
        //         $rows = $worksheet->toArray();

                
        //         print_r('<pre>');
        //         print_r($rows);
        //         print_r('</pre>');

        //         foreach ($rows as $row) {
        //             $data = Customer::find()->where(['firstname' => $row[0]])->andWhere(['lastname' => $row[])
        //         }

        //         die;
        //         foreach ($rows as $row) 
        //         {
        //             array_push($importing_items, $row[0]);
        //         }
                
        //         $items_to_add = array_diff($importing_items,$existing_items);
                    
        //         foreach ($items_to_add as $item) 
        //         {
        //             $import = new MaterialType();
        //             $import->name = $item;
        //             $import->save();
                    
        //         }
              
        //     } 
  
        // }

        // // ---------------- УДАЛЕНИЕ ВСЕХ ЭЛЕМЕНТОВ СПРАВОЧНИКА ---------------
        // if ($delete_form->load(Yii::$app->request->post()) && $delete_form->validate()) 
        // {
        //     die;
        //     $data = MaterialType::deleteAll();
            
        // }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'update_form' => $update_form,
            'delete_form' => $delete_form,

        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
