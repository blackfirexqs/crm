<?php

namespace app\controllers;

use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\dict\Color;
use app\models\dict\ColorSearch;
use app\models\dict\MaterialType;
use app\models\dict\ColorToMaterialType;
use app\models\form\UpdateDictForm;
use app\models\form\DeleteDictForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DictColorController implements the CRUD actions for Color model.
 */
class DictColorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Color models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ColorSearch();
        $update_form = new UpdateDictForm();
        $delete_form = new DeleteDictForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

       // ---------------- ДОБАВЛЕНИЕ ЭЛЕМЕНТОВ СПРАВОЧНИКА ИЗ ФАЙЛА ---------------
        if ($update_form->load(Yii::$app->request->post()) && $update_form->validate()) 
        {
            $file_path = realpath(dirname(__FILE__).'/../files/dict/color.xlsx');

            if (file_exists($file_path)) 
            {
                $importing_items = [];
                $existing_items = [];

                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($file_path);
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();

                // ---- Проверяем существующие типы материалов, и добавляем если есть новые
                $color_table = Color::find()->asArray()->all();
                foreach ($color_table as $ct) {
                    array_push($existing_items, $ct['name']);
                }

                foreach ($rows as $row) 
                {  

                    if ($row[0])
                    {

                        array_push($importing_items, $row[0]);
                    }
                }

                $items_to_add = array_unique(array_diff($importing_items,$existing_items));

                if(!empty($items_to_add)) 
                {

                    foreach ($items_to_add as $ita) {
                        $data = new Color();
                        $data->name = $ita;
                     
                        $data->save();
                    }
                }

            }

        }
       
        // ---------------- УДАЛЕНИЕ ВСЕХ ЭЛЕМЕНТОВ СПРАВОЧНИКА ---------------
        if ($delete_form->load(Yii::$app->request->post()) && $delete_form->validate()) 
        {
            $data = Color::deleteAll();
            $data = ColorToMaterialType::deleteAll();
            
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'update_form' => $update_form,
            'delete_form' => $delete_form,

        ]);
    }

    /**
     * Displays a single Color model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Color model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Color();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->color_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Color model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->color_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Color model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Color model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Color the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Color::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
