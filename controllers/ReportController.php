<?php

namespace app\controllers;

use Yii;

use yii\filters\AccessControl;

use yii\web\Controller;

use yii\web\Response;

use yii\filters\VerbFilter;

use yii\db\ActiveRecord; 

use app\models\User;

use app\models\form\CustomReportForm;

use app\models\general\OrderOperation;

use app\models\dict\Operation;

use DateTime;

class ReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                
                'rules' => [
                    [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@'],
                    ],

                   
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [

            'error' => [

                'class' => 'yii\web\ErrorAction',

            ],

            'captcha' => [

                'class' => 'yii\captcha\CaptchaAction',

                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,

            ],

        ];
    }

    public function actionIndex()

    {
        $date = new DateTime;

        
        if (Yii::$app->request->get('Report')) {

        }
        



        $first_of_prev_month = date("Y-m-d 00:00:00", strtotime("first day of last month")); 
        $last_of_prev_month = date("Y-m-d 23:59:59", strtotime("last day of last month"));

        $prev_month_total = array();
        $this_month_total = array();
        $prev_month = OrderOperation::find()->where(['>=','date', $first_of_prev_month])->andWhere(['<=','date',$last_of_prev_month])->asArray()->all();

        $total = array(); 

        $rezka_id_obj = Operation::find()->where(['OR',['like','name', 'резка'],['like','name', 'Резка']])->andWhere(['OR',['not like','name', 'обрезка'],['not like','name', 'Обрезка']])->asArray()->all();
        $rezka_id_arr = [];
        foreach ($rezka_id_obj as $rio) {
        	array_push($rezka_id_arr, $rio['operation_id']);
        }
       

        foreach ($prev_month as $pm) {
            if($pm['user_id'])
            {
                $total[$pm['user_id']] = 0;
            }
            if($pm['user_id_2'])
            {
                $total[$pm['user_id_2']] = 0;
            }
        }
        foreach ($prev_month as $pm) {

          




            if($pm['user_id'])
            {
                if(in_array($pm['operation_id'],$rezka_id_arr) && $pm['user_id_2'])
                {
                    $total[$pm['user_id']] += $pm['cost'] * 0.65;
                } elseif (in_array($pm['operation_id'],$rezka_id_arr)) {
                    $total[$pm['user_id']] += $pm['cost'];
                } elseif ($pm['user_id_2']) {
                    $total[$pm['user_id']] += $pm['cost'] * 0.5; 
                } else {
                    $total[$pm['user_id']] += $pm['cost'];
                }
                
            }

            if($pm['user_id_2'])
            {
                if(in_array($pm['operation_id'],$rezka_id_arr)){
                    $total[$pm['user_id_2']] += $pm['cost'] * 0.35;
                } else
                {
                    $total[$pm['user_id_2']] += $pm['cost'] * 0.5;
                }
            }
        }

        foreach ($total as $key => $value) {
            $user_data = User::find()->where(['user_id' => $key])->one();
            if($user_data)
            {
                $full_name = $user_data->lastname.' '.$user_data->firstname.' '.$user_data->patronymic;
            } else
            {
                $full_name = '';
            }
            $prev_month_total[] = array(
                'name' => $full_name,
                'salary' => $value,
            );

        }

        $first_of_this_month = date("Y-m-d 00:00:00", strtotime("first day of this month"));
        $last_of_this_month = date("Y-m-d 23:59:59", strtotime("last day of this month"));
        
        $this_month = OrderOperation::find()->where(['>=','date', $first_of_this_month])->andWhere(['<=','date',$last_of_this_month])->asArray()->all();

        $total = array(); 
        foreach ($this_month as $tm) {
            if($tm['user_id'])
            {
                $total[$tm['user_id']] = 0;
            }
            if($tm['user_id_2'])
            {
                $total[$tm['user_id_2']] = 0;
            }
        }

        

        foreach ($this_month as $tm) {
            
            if($tm['user_id'])
            {

                if(in_array($tm['operation_id'],$rezka_id_arr) && $tm['user_id_2'])
                {
                    $total[$tm['user_id']] += $tm['cost'] * 0.65;
                } elseif (in_array($tm['operation_id'],$rezka_id_arr)) {
                    $total[$tm['user_id']] += $tm['cost'];
                } elseif ($tm['user_id_2']) {
                    $total[$tm['user_id']] += $tm['cost'] * 0.5; 
                } else {
                    $total[$tm['user_id']] += $tm['cost'];
                }
                
            }

            if($tm['user_id_2'])
            {
                if(in_array($tm['operation_id'],$rezka_id_arr)){
                    $total[$tm['user_id_2']] += $tm['cost'] * 0.35;
                } else
                {
                    $total[$tm['user_id_2']] += $tm['cost'] * 0.5;
                }
            }
        }

        foreach ($total as $key => $value) {
            $user_data = User::find()->where(['user_id' => $key])->one();
            if($user_data)
            {
                $full_name = $user_data->lastname.' '.$user_data->firstname.' '.$user_data->patronymic;
            } else
            {
                $full_name = '';
            }
            $this_month_total[] = array(
                'name' => $full_name,
                'salary' => $value,
            );

        }
        

        $custom_report_form = new CustomReportForm();
        $custom_report_total = array();
        if ($custom_report_form->load(Yii::$app->request->get()) && $custom_report_form->validate()) {
            
            $custom_report = OrderOperation::find()->where(['>=','date', $custom_report_form->date_from])->andWhere(['<=','date',$custom_report_form->date_to.' 23:59:59'])->asArray()->all();

             $total = array(); 
            foreach ($custom_report as $cr) {
                if($cr['user_id'])
                {
                    $total[$cr['user_id']] = 0;
                }
                if($cr['user_id_2'])
                {
                    $total[$cr['user_id_2']] = 0;
                }
            }

            

            foreach ($custom_report as $cr) {
                
                if($cr['user_id'])
                {

                    if(in_array($cr['operation_id'],$rezka_id_arr) && $cr['user_id_2'])
                    {
                        $total[$cr['user_id']] += $cr['cost'] * 0.65;
                    } elseif (in_array($cr['operation_id'],$rezka_id_arr)) {
                        $total[$cr['user_id']] += $cr['cost'];
                    } elseif ($cr['user_id_2']) {
                        $total[$cr['user_id']] += $cr['cost'] * 0.5; 
                    } else {
                        $total[$cr['user_id']] += $cr['cost'];
                    }
                    
                }

                if($cr['user_id_2'])
                {
                    if(in_array($cr['operation_id'],$rezka_id_arr)){
                        $total[$cr['user_id_2']] += $cr['cost'] * 0.35;
                    } else
                    {
                        $total[$cr['user_id_2']] += $cr['cost'] * 0.5;
                    }
                }
            }

            foreach ($total as $key => $value) {
                $user_data = User::find()->where(['user_id' => $key])->one();
                if($user_data)
                {
                    $full_name = $user_data->lastname.' '.$user_data->firstname.' '.$user_data->patronymic;
                } else
                {
                    $full_name = '';
                }
                $custom_report_total[] = array(
                    'name' => $full_name,
                    'salary' => $value,
                );

            }

            

        }


        return $this->render('index', ['prev_month_total' => $prev_month_total,'this_month_total' => $this_month_total,'custom_report_form' => $custom_report_form,
            'custom_report_total' => $custom_report_total]);
    }











}

	


?>
