<?php

namespace app\controllers;

use Yii;
use app\models\dict\SecondaryMaterial;
use app\models\dict\SecondaryMaterialSearch;
use yii\web\Controller;
use app\models\form\UpdateDictForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DictSecondaryMaterialController implements the CRUD actions for SecondaryMaterial model.
 */
class DictSecondaryMaterialController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SecondaryMaterial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SecondaryMaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $update_form = new UpdateDictForm;
         // ---------------- ДОБАВЛЕНИЕ ЭЛЕМЕНТОВ СПРАВОЧНИКА ИЗ ФАЙЛА ---------------
        if ($update_form->load(Yii::$app->request->post()) && $update_form->validate()) 
        {
            $file_path = realpath(dirname(__FILE__).'/../files/dict/secondary_material.xlsx');
            if (file_exists($file_path)) 
            {
                $importing_items = [];
                $existing_items = [];

                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($file_path);
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();

                // ---- Проверяем существующие типы материалов, и добавляем если есть новые
                $secondary_material_table = SecondaryMaterial::find()->asArray()->all();
                foreach ($secondary_material_table as $smt) {
                    array_push($existing_items, $smt['name']);
                }
                
                foreach ($rows as $row) 
                {  

                    if ($row[0])
                    {

                        array_push($importing_items, $row[0]);
                    }
                }



                $items_to_add = array_unique(array_diff($importing_items,$existing_items));


                if(!empty($items_to_add)) 
                {

                    foreach ($items_to_add as $ita) {
                        $data = new SecondaryMaterial();
                        $data->name = $ita;
                     
                        $data->save();
                    }
                }
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'update_form' => $update_form,
        ]);
    }

    /**
     * Displays a single SecondaryMaterial model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SecondaryMaterial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SecondaryMaterial();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->secondary_material_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SecondaryMaterial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->secondary_material_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SecondaryMaterial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SecondaryMaterial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SecondaryMaterial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SecondaryMaterial::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
