<?php


use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
$this->title = 'Отчеты';
?>

<div class="report-index">

	<div class="row">
		<div class="col-md-6">
			<h2>Заработная плата за прошедший месяц</h2>
			<br>
			<table class="table table-striped table-bordered">
		    	<thead>
		    		<tr>
		    			<th>Сотрудник</th>
		    			<th>Заработная плата</th>
		    		</tr>
		    	</thead>
		    	<tbody>
					<?php foreach ($prev_month_total as $pmt) {
						
					 ?>
		    		<tr>
		    			<td>
		    				<?php echo $pmt['name']; ?>
		    			</td>
		    			<td>
		    				<?php echo $pmt['salary'].' руб.'; ?>
		    			</td>
		    		</tr>
					<?php } ?>
		    	</tbody>

	    	</table>
		</div>

		<div class="col-md-6">
			<h2>Заработная плата за текущий месяц</h2>
			<br>
			<table class="table table-striped table-bordered">
		    	<thead>
		    		<tr>
		    			<th>Сотрудник</th>
		    			<th>Заработная плата</th>
		    		</tr>
		    	</thead>
		    	<tbody>
					<?php foreach ($this_month_total as $pmt) {
						
					 ?>
		    		<tr>
		    			<td>
		    				<?php echo $pmt['name']; ?>
		    			</td>
		    			<td>
		    				<?php echo $pmt['salary'].' руб.'; ?>
		    			</td>
		    		</tr>
					<?php } ?>
		    	</tbody>

	    	</table>
		</div>
	</div>
   	<hr>
		<div class="row">

			<div class="col-md-12">
				<h2>Заработная плата за выбранный промежуток времени</h2>
				<br>
			</div>
				
				<?php $form = ActiveForm::begin([
		        'action' => ['index'],
		        'method' => 'get',
		    	]); ?>
			<div class="col-md-6">

				<?= $form->field($custom_report_form, 'date_from')->widget(\yii\jui\DatePicker::class, [
		                'language' => 'ru',
		                'dateFormat' => 'yyyy-MM-dd',
		                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
		            ]) ?>

			</div>
			<div class="col-md-6">
		        <?= $form->field($custom_report_form, 'date_to')->widget(\yii\jui\DatePicker::class, [
		                'language' => 'ru',
		                'dateFormat' => 'yyyy-MM-dd',
		                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
		            ]) ?>
			</div>
		    <div class="col-md-12">
		       	<?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>

		    </div>
	
		        <?php ActiveForm::end(); ?>
	        

		</div>

	<?php if ($custom_report_total) { ?>
		<div class="row">
			<div class="col-md-12">
				<br>
				
				<table class="table table-striped table-bordered">
			    	<thead>
			    		<tr>
			    			<th>Сотрудник</th>
			    			<th>Заработная плата</th>
			    		</tr>
			    	</thead>
			    	<tbody>
						<?php foreach ($custom_report_total as $crt) {
							
						 ?>
			    		<tr>
			    			<td>
			    				<?php echo $crt['name']; ?>
			    			</td>
			    			<td>
			    				<?php echo $crt['salary'].' руб.'; ?>
			    			</td>
			    		</tr>
						<?php } ?>
			    	</tbody>

		    	</table>
			</div>
		</div>

	<?php }?>
   
</div>