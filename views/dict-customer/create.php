<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\dict\Customer */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Заказчики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
