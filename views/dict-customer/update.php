<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\Customer */

$this->title = $model->firstname.' '.$model->lastname.' '.$model->patronymic;
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Заказчики', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstname.' '.$model->lastname.' '.$model->patronymic, 'url' => ['view', 'id' => $model->customer_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="customer-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
