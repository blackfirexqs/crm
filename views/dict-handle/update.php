<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\Handle */

$this->title = 'Изменить ручку: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Ручки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->handle_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="handle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
