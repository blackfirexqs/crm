<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\dict\Handle */

$this->title = 'Create Handle';
$this->params['breadcrumbs'][] = ['label' => 'Handles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="handle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
