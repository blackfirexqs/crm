<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">


    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p class="lead">Вы успешно авторизировались в системе внутреннего учета мебельной фабрики Victoria Ricci.</p>
        <p class="lead">Теперь можете ознакомиться с документацией по использованию системы.</p>

        <p><a class="btn btn-lg btn-success" href="#">Посмотреть документацию</a></p>
    </div>

   
</div>
