<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Словари';
?>
<div class="site-index">

<a href="dict-color/index"><button class="btn" >Цвета</button></a>

<a href="dict-material-type/index"><button class="btn" >Типы материалов</button></a>

<a href="dict-customer/index"><button class="btn" >Заказчики</button></a>

<a href="dict-operation/index"><button class="btn" >Операции</button></a>

<a href="dict-order-type/index"><button class="btn" >Типы заказов</button></a>

<a href="dict-product-type/index"><button class="btn" >Типы изделий</button></a>

<a href="dict-handle/index"><button class="btn" >Ручки</button></a>

<a href="dict-user/index"><button class="btn" >Пользователи</button></a>

<a href="dict-main-material/index"><button class="btn" >Основной материал</button></a>

<a href="dict-secondary-material/index"><button class="btn" >Дополнительный материал</button></a>

</div>
