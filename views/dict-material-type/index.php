<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\dict\MaterialTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы материалов';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-type-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dict-button-group">
    <?= Html::a('Создать новую запись', ['create'], ['class' => 'btn btn-success']) ?>

  

    <?php $del_form = ActiveForm::begin(); ?>
        <?= Html::submitButton('Удалить все записи', ['class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Вы действительно хотите удалить все записи из таблицы?\')']) ?>
        <?php echo $del_form->field($delete_form, 'delete_button')->label(false)->hiddenInput(['value' => 'delete_button']); ?>
    <?php ActiveForm::end(); ?>

    <p class="red-info">Добавление элементов осуществляется автоматически при добавлении элементов справочника Цветов </p>

    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'material_type_id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
