<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\dict\MaterialType */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Типы материалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
