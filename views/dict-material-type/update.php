<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\MaterialType */

$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Типы материалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->material_type_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="material-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
