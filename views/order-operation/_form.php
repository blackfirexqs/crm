<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\dict\MaterialType;
use app\models\dict\Operation;
use app\models\general\Order;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\general\OrderOperation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-operation-form">

    <?php $form = ActiveForm::begin();

    $params = [
        'prompt' => '-'
    ];

    

    $users = User::find()->all();
    //print_r($users);
    foreach ($users as $user) {
        $user['firstname'] = $user['lastname'].' '.$user['firstname'].' '.$user['patronymic'];
    }
    $items = ArrayHelper::map($users,'user_id','firstname');
    echo $form->field($model, 'user_id')->dropDownList($items,$params); 
    echo $form->field($model, 'user_id_2')->dropDownList($items,$params);
    if($model->date != '0000-00-00 00:00:00') {
    echo $form->field($model, 'date')->textInput(); 
    }
    
    ?>

   

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
