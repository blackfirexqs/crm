<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\general\OrderOperation */
use app\models\general\Order;
$order_name = Order::find()->where(['order_id' => $model->order_id])->asArray()->one();

$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['order/index']];
$this->params['breadcrumbs'][] = ['label' => $order_name['order_general_id'], 'url' => ['order/'.$model->order_id]];
$this->params['breadcrumbs'][] = 'Изменить операцию';
?>
<div class="order-operation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
