<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\general\OrderOperation */

$this->title = 'Create Order Operation';
$this->params['breadcrumbs'][] = ['label' => 'Order Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-operation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
