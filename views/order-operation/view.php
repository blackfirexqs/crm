<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\general\OrderOperation */

$this->title = $model->order_operation_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-operation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_operation_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_operation_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_operation_id',
            'material_type_id',
            'operation_id',
            'order_id',
            'cost',
            'user_id',
            'user_id_2',
            'date',
            'description',
        ],
    ]) ?>

</div>
