<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\general\OrderOperationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-operation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_operation_id') ?>

    <?= $form->field($model, 'material_type_id') ?>

    <?= $form->field($model, 'operation_id') ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
