<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\dict\ColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цвета';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dict-button-group">
    <?= Html::a('Создать новую запись', ['create'], ['class' => 'btn btn-success']) ?>

    <?php $upd_form = ActiveForm::begin(); ?>
        <?= Html::submitButton('Добавить записи из файла', ['class' => 'btn btn-primary', 'onclick' => 'return confirm(\'Вы действительно хотите добавить новые записи из файла?\')']) ?>
        <?php echo $upd_form->field($update_form, 'update_button')->label(false)->hiddenInput(['value' => 'update_button']); ?>
    <?php ActiveForm::end(); ?>

    <?php $del_form = ActiveForm::begin(); ?>
        <?= Html::submitButton('Удалить все записи', ['class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Вы действительно хотите удалить все записи из таблицы?\')']) ?>
        <?php echo $del_form->field($delete_form, 'delete_button')->label(false)->hiddenInput(['value' => 'delete_button']); ?>
    <?php ActiveForm::end(); ?>

    <p class="red-info">Добавление элементов осуществляется из файла находящего в "..files/dict/color.xlsx" </p>

    </div>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'color_id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

    <script>
      
    </script>