<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\dict\Color */

$this->title = 'Создать запись';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Цвета', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
