<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\Color */


$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = ['label' => 'Цвета', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->color_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="color-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
