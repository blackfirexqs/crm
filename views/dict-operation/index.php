<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\dict\OperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Операции';
$this->params['breadcrumbs'][] = ['label' => 'Справочники', 'url' => ['/dictionary']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dict-button-group">
        <p>
            <?= Html::a('Создать новую запись', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'operation_id',
            'name',
            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
