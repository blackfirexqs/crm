<?php

use yii\helpers\Html;
use app\models\general\Order;

$order_name = Order::find()->where(['order_id' => $model->order_id])->one();
$order_name = $order_name['order_general_id'];

/* @var $this yii\web\View */
/* @var $model app\models\general\Order */

$this->title = 'Изменить заказ';
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $order_name, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="order-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
