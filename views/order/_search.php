<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\dict\Customer;
use app\models\dict\OrderType;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\general\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // echo $form->field($model, 'order_id') ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'order_local_id') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'order_general_id') ?>
        </div>
        <div class="col-sm-4">
            
            <?php 
            //выпадающий список с заказчиками
            $items = array();
            $elements = Customer::find()->asArray()->all();
            foreach ($elements as $e) 
            {
               $items[$e['customer_id']] = $e['firstname'].' '.$e['lastname'].' '.$e['patronymic'];
            }
            $params = [
                'prompt' => 'Выберите заказчика'
            ];
            ?>
            <?= $form->field($model, 'customer_id')->dropDownList($items,$params); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">

            <?php 
            //выпадающий список с типом заказа
            $items = array();
            $elements = OrderType::find()->asArray()->all();
            foreach ($elements as $e) 
            {
               $items[$e['order_type_id']] = $e['name'];
            }
            $params = [
                'prompt' => 'Выберите тип заказа'
            ];
            ?>
            <?= $form->field($model, 'order_type_id')->dropDownList($items,$params); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'date_in')->widget(\yii\jui\DatePicker::class, [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
            ]) ?>
        </div>
        <div class="col-sm-4">

            <?= $form->field($model, 'date_out')->widget(\yii\jui\DatePicker::class, [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
            ]) ?>
        </div>
    </div>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'prepayment') ?>

    <?php // echo $form->field($model, 'postpayment') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'product_type_id') ?>

    <?php // echo $form->field($model, 'facade_top_main_id') ?>

    <?php // echo $form->field($model, 'facade_top_extra_id') ?>

    <?php // echo $form->field($model, 'facade_bottom_main_id') ?>

    <?php // echo $form->field($model, 'facade_bottom_extra_id') ?>

    <?php // echo $form->field($model, 'custom') ?>

    <?php // echo $form->field($model, 'radius') ?>

    <?php // echo $form->field($model, 'frame_color_id') ?>

    <?php // echo $form->field($model, 'glass_color_id') ?>

    <?php // echo $form->field($model, 'mirror_color_id') ?>

    <?php // echo $form->field($model, 'table_top_color_id') ?>

    <?php // echo $form->field($model, 'wall_panel_color_id') ?>

    <?php // echo $form->field($model, 'kant_color_id') ?>

    <?php // echo $form->field($model, 'base_color_id') ?>

    <?php // echo $form->field($model, 'dry_material_type_id') ?>

    <?php // echo $form->field($model, 'dry_size') ?>

    <?php // echo $form->field($model, 'wash_material_type_id') ?>

    <?php // echo $form->field($model, 'wash_size') ?>

    <?php // echo $form->field($model, 'tray_size') ?>

    <?php // echo $form->field($model, 'handle_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
