<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\general\Order;
use app\models\dict\Customer;
use app\models\dict\OrderType;
use app\models\dict\ProductType;
use app\models\dict\MaterialType;
use app\models\dict\Color;
use app\models\dict\Handle;

$order_name = Order::find()->where(['order_id' => $model->order_id])->one();
$order_name = $order_name['order_general_id'];

/* @var $this yii\web\View */
/* @var $model app\models\general\Order */

$this->title = $order_name;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

 

    <div class="dict-button-group">
        <?= Html::a('Изменить', ['update', 'id' => $model->order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить этот элемет?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
     <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#operations">Операции</a></li>
        <li><a data-toggle="tab" href="#info">Информация о заказе</a></li>

    </ul>

    <div class="tab-content">
      <div id="operations" class="tab-pane fade in active">
         
            <?php $current_material_type_id = 0; 
            $index = 0;
            ?>
            <?php foreach ($order_operations as $order_operation_values) { ?>
                <div class="panel panel-primary">
                <?php foreach ($order_operation_values as $key => $order_operation) { ?>
                    <?php if (!$key) { ?>
                        <div class="panel-heading">Тип материала: <?php echo $order_operation['material_type_name']; ?></div>
                    <?php } ?>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <strong style="font-weight: bold !important;"><?php echo $order_operation['operation_name']; ?></strong><br>
                                <?php if ($order_operation['user_name'] || $order_operation['user_2_name']) { ?>
                                    <?php if ($order_operation['user_name']) { ?>
                                    <?php echo $order_operation['user_name'] ? $order_operation['user_name'] : 'Не назначен' ?>
                                    <?php } ?>
                                    <?php if ($order_operation['user_2_name']) { ?>
                                    / <?php echo $order_operation['user_2_name'] ? $order_operation['user_2_name'] : 'Не назначен' ?><br>
                                    <?php } ?> 
                                <?php } ?>
                            </div>
                            <div class="col-md-2">
                                <?php if (!($order_operation['user_name'] || $order_operation['user_2_name'])) { ?>
                                <span class="glyphicon glyphicon-question-sign text-danger"></span>
                                <?php } ?>
                            </div>
                            <div class="col-md-2">
                                <a href="/order-operation/update?id=<?php echo $order_operation['order_operation_id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
        
        
      </div>
      <div id="info" class="tab-pane fade">
       
      
      </div>
     
    </div>


 

</div>
