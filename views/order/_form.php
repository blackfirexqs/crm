<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\dict\Customer;
use app\models\dict\OrderType;
use app\models\dict\ProductType;
use app\models\dict\MaterialType;
use app\models\dict\Color;
use app\models\dict\Handle;
use app\models\dict\MainMaterial;
use app\models\dict\SecondaryMaterial;
use yii\helpers\ArrayHelper;
use yii\jui\AutoComplete;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\general\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
    	<div class="col-sm-6">
	    	<?= $form->field($model, 'order_general_id')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-6">
		    <?= $form->field($model, 'order_local_id')->textInput(['maxlength' => true]) ?>
		</div>
    </div>

    <div class="row well">
    	<div class="col-sm-4">
		    <?php 
		    //выпадающий список с заказчиками
		    $items = array();
		    $elements = Customer::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['customer_id']] = $e['firstname'].' '.$e['lastname'].' '.$e['patronymic'];
		    }
		    $params = [
		        'prompt' => 'Укажите заказчика'
		    ];
		    echo $form->field($model, 'customer_id')->dropDownList($items,$params);
		    ?>
		</div>
    	<div class="col-sm-4">
		    <?php 
		    //выпадающий список с типом заказа
		    $items = array();
		    $elements = OrderType::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['order_type_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип заказа'
		    ];
		    echo $form->field($model, 'order_type_id')->dropDownList($items,$params);
		    ?>
		</div>
		<div class="col-sm-4">
		    <?php 
		    //выпадающий список с видом изделия
		    $items = array();
		    $elements = ProductType::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['product_type_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите вид изделия'
		    ];
		    echo $form->field($model, 'product_type_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-4">
    		<?= $form->field($model, 'date_in')->textInput() ?>
    	</div>
    	<div class="col-sm-4">
    		<?= $form->field($model, 'date_out')->textInput() ?>
    	</div>
    	<div class="col-sm-4">
    		<?= $form->field($model, 'date_out_real')->textInput() ?>
    	</div>
    </div>

    <div class="row well">
    	<div class="col-sm-4">
    		<?= $form->field($model, 'total')->textInput() ?>
    	</div>
    	<div class="col-sm-4">
			<?= $form->field($model, 'prepayment')->textInput() ?>
    	</div>
    	<div class="col-sm-4">
			<?= $form->field($model, 'postpayment')->textInput() ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-4">

		    <?php 
		    //выпадающий список со статусами
		    $items = array();
		    $items[0] = 'В работе';
		    $items[1] = 'Заказ готов, но не отгружен';
		    $items[2] = 'Заказ отправлен';
		    
		    echo $form->field($model, 'status')->dropDownList($items);
		    ?>

    	</div>

    	<div class="col-sm-8">
    		<?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
    	</div>
    </div>

    

   

    <div class="row well">
    	<div class="col-sm-4">
		    <?php 
		    //выпадающий список с основной верхней частью фасада
		    $items = array();
		    $elements = MainMaterial::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['main_material_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип основного материала'
		    ];
		    echo $form->field($model, 'facade_top_main_id')->dropDownList($items,$params);
		    ?>
		</div>

    	<div class="col-sm-4">
    		<?php 
		    
		    //список с автозаполнением дополнительного материала верхней части фасада
		    $elements = SecondaryMaterial::find()->select(['name as value', 'name as label'])->asArray()->all();
		    echo $form->field($model, 'facade_top_extra_id')->widget(
		    AutoComplete::className(), [            
		        'clientOptions' => [
		        	'autoFill'=>true,
		            'source' => $elements,
		        ],
		      
		        'options'=>[
		            'class'=>'form-control',
		       

		        ]
		    ]);?>
		</div>
		<div class="col-sm-4">
			<?php
		    //список с автозаполнением цветов
		    $elements = Color::find()->select(['name as value', 'name as label'])->asArray()->all();

		    echo $form->field($model, 'facade_top_color_id')->widget(
		    AutoComplete::className(), [            
		        'clientOptions' => [
		        	'autoFill'=>true,
		            'source' => $elements,
		        ],
		      
		        'options'=>[
		            'class'=>'form-control',
		       

		        ]
		    ]);

		    //echo $form->field($model, 'facade_top_extra_id')->dropDownList($items,$params);
		    ?>
		</div>

		<div class="col-sm-6">

			 <?php 
		    //выпадающий список со статусами
		    $items = array();
		    $items[0] = ' ';
		    $items[1] = 'Да';
		    $items[2] = 'Нет';
		    
		    echo $form->field($model, 'facade_top_custom')->dropDownList($items);
		    ?>

		</div>

		<div class="col-sm-6">

			 <?= $form->field($model, 'facade_top_date')->widget(\yii\jui\DatePicker::class, [
		                'language' => 'ru',
		                'dateFormat' => 'yyyy-MM-dd',
		                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
		            ]) ?>
		    

		</div>
    	

    </div>


    <div class="row well">
    	<div class="col-sm-4">
		    <?php 
		    //выпадающий список с основной верхней частью фасада
		    $items = array();
		    $elements = MainMaterial::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['main_material_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип основного материала'
		    ];
		    echo $form->field($model, 'facade_bottom_main_id')->dropDownList($items,$params);
		    ?>
		</div>

		<div class="col-sm-4">
    		<?php 
		    
		    //список с автозаполнением дополнительного материала верхней части фасада
		    $elements = SecondaryMaterial::find()->select(['name as value', 'name as label'])->asArray()->all();
		    echo $form->field($model, 'facade_bottom_extra_id')->widget(
		    AutoComplete::className(), [            
		        'clientOptions' => [
		        	'autoFill'=>true,
		            'source' => $elements,
		        ],
		      
		        'options'=>[
		            'class'=>'form-control',
		       

		        ]
		    ]);?>
		</div>

		<div class="col-sm-4">
			<?php
		    //список с автозаполнением цветов
		    $elements = Color::find()->select(['name as value', 'name as label'])->asArray()->all();

		    echo $form->field($model, 'facade_bottom_color_id')->widget(
		    AutoComplete::className(), [            
		        'clientOptions' => [
		        	'autoFill'=>true,
		            'source' => $elements,
		        ],
		      
		        'options'=>[
		            'class'=>'form-control',
		       

		        ]
		    ]);

		    //echo $form->field($model, 'facade_top_extra_id')->dropDownList($items,$params);
		    ?>
		</div>

		<div class="col-sm-6">

			 <?php 
		    //выпадающий список со статусами
		    $items = array();
		    $items[0] = ' ';
		    $items[1] = 'Да';
		    $items[2] = 'Нет';
		    
		    echo $form->field($model, 'facade_bottom_custom')->dropDownList($items);
		    ?>

		</div>

		<div class="col-sm-6">

			 <?= $form->field($model, 'facade_bottom_date')->widget(\yii\jui\DatePicker::class, [
			 			'value' => '2222-22-22',
		                'language' => 'ru',
		                'dateFormat' => 'yyyy-MM-dd',
		                'options' => ['class' => 'form-control', 'autocomplete' => 'off']
		            ]) ?>
		    

		</div>

	</div>

    <?php 
    //выпадающий список с выбором заказная позиция или нет
    /*$items = array();
    $items[0] = 'Не определено';
    $items[1] = 'Нет';
    $items[2] = 'Да';
    
    echo $form->field($model, 'custom')->dropDownList($items);*/
    ?>

    <?php 
    //выпадающий список с выбором есть ли радиусная мебель или нет
    /*$items = array();
    $items[0] = 'Не определено';
    $items[1] = 'Нет';
    $items[2] = 'Да';
    
    echo $form->field($model, 'radius')->dropDownList($items);*/
    ?>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с цветами для каркаса
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет каркаса'
		    ];
		    echo $form->field($model, 'frame_color_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

	<div class="row well">

    	<div class="col-sm-6">

		    <?php 
		    //выпадающий список с цветами для стекла
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет стекла'
		    ];
		    echo $form->field($model, 'glass_color_id')->dropDownList($items,$params);
		    ?>
		</div>
		<div class="col-sm-6">
		    <?php 
		    //выпадающий список с цветами для зеркала
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет зеркала'
		    ];
		    echo $form->field($model, 'mirror_color_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-6">
		    <?php 
		    //выпадающий список с цветами для столешницы
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет столешницы'
		    ];
		    echo $form->field($model, 'table_top_color_id')->dropDownList($items,$params);
		    ?>
		</div>
    	<div class="col-sm-6">

		    <?= $form->field($model, 'table_top_size')->textInput() ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с цветами для стеновой панели
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет стеновой панели'
		    ];
		    echo $form->field($model, 'wall_panel_color_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с цветами для канта
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет канта'
		    ];
		    echo $form->field($model, 'kant_color_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с цветами для цоколя
		    $items = array();
		    $elements = Color::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['color_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите цвет цоколя'
		    ];
		    echo $form->field($model, 'base_color_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>


    <div class="row well">
    	<div class="col-sm-6">
		    <?php 
		    //выпадающий список с типами материалов для сушки
		    $items = array();
		    $elements = MaterialType::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['material_type_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип материала'
		    ];
		    echo $form->field($model, 'dry_type_id')->dropDownList($items,$params);
		    ?>
		</div>
    	<div class="col-sm-6">

    		<?= $form->field($model, 'dry_size')->textInput() ?>
    	</div>
    </div>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с типами материалов для мойки
		    $items = array();
		    $elements = MaterialType::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['material_type_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип материала'
		    ];
		    echo $form->field($model, 'wash_type_id')->dropDownList($items,$params);
		    ?>
		</div>
    	
	</div>


    <div class="row well">
    	<div class="col-sm-12">
    		<?= $form->field($model, 'tray_size')->textInput() ?>
    	</div>
    </div>

    <div class="row well">
    	<div class="col-sm-12">
		    <?php 
		    //выпадающий список с ручками
		    $items = array();
		    $elements = Handle::find()->asArray()->all();
		    foreach ($elements as $e) 
		    {
		       $items[$e['handle_id']] = $e['name'];
		    }
		    $params = [
		        'prompt' => 'Укажите тип ручки'
		    ];
		    echo $form->field($model, 'handle_type_id')->dropDownList($items,$params);
		    ?>
		</div>
	</div>

	<style>
		.row.well {
			border-color: #000;
		}
	</style>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
