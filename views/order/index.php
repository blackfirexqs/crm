<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\dict\Customer;
use app\models\dict\OrderType;
use app\models\general\OrderOperation;
use app\models\dict\ProductType;



/* @var $this yii\web\View */
/* @var $searchModel app\models\general\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">


    <div class="dict-button-group">
        <?= Html::a('Создать новую запись', ['create'], ['class' => 'btn btn-success']) ?>

        <?php $upd_form = ActiveForm::begin(); ?>
        <?= Html::submitButton('Добавить записи из файла', ['class' => 'btn btn-primary', 'onclick' => 'return confirm(\'Вы действительно хотите добавить новые записи из файла?\')']) ?>
        <?php echo $upd_form->field($update_form, 'update_button')->label(false)->hiddenInput(['value' => 'update_botton']); ?>
        <?php ActiveForm::end(); ?>
    </div>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions'=>function($model){
            if($model->status == 1){
                return ['class' => 'success'];
            }
            if($model->status == 2){
                return ['class' => 'dark'];
            }
        },
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'order_id',
            'order_local_id',
            'order_general_id',
            [
                'attribute' => 'customer_id',
                'format' => 'raw',
                'value' => function($model)
                {
                    $element = Customer::find()->where(['customer_id' => $model->customer_id])->asArray()->one();
                    if ($element)
                    {
                        return $element['firstname'].' '.$element['lastname'].' '.$element['patronymic'] ;
                    } 
                }
            ],
            
            
            
            'date_in',

            [
                'attribute' => 'order_type_id',
                'format' => 'raw',
                'value' => function($model)
                {
                    $element = OrderType::find()->where(['order_type_id' => $model->order_type_id])->asArray()->one();
                    if ($element)
                    {
                        return $element['name'];
                    } 
                }
            ],
            //'date_out',
            //'total',
            //'prepayment',
            //'postpayment',
            //'comment',
            [
                'attribute' => 'product_type_id',
                'format' => 'raw',
                'value' => function($model)
                {
                    $element = ProductType::find()->where(['product_type_id' => $model->product_type_id])->asArray()->one();
                    if ($element)
                    {
                        return $element['name'];
                    } 
                }
            ],

            'date_out',
            'date_out_real',

            [
                'attribute' => 'Процент',
                'format' => 'raw',
                'value' => function($model)
                {
                    $all_operations = OrderOperation::find()->where(['order_id' => $model->order_id])->asArray()->all();
                    $complete_operations = OrderOperation::find()->where(['order_id' => $model->order_id])->andWhere(['not', ['user_id' => '0']])->asArray()->all();
                    if ($all_operations && $complete_operations)
                    {
                        return round((count($complete_operations) / count($all_operations)) * 100) . ' %';
                    } else {
                        return '0 %';
                    }
                }
            ],
            //'facade_top_main_id',
            //'facade_top_extra_id',
            //'facade_bottom_main_id',
            //'facade_bottom_extra_id',
            //'custom',
            //'radius',
            //'frame_color_id',
            //'glass_color_id',
            //'mirror_color_id',
            //'table_top_color_id',
            //'wall_panel_color_id',
            //'kant_color_id',
            //'base_color_id',
            //'dry_material_type_id',
            //'dry_size',
            //'wash_material_type_id',
            //'wash_size',
            //'tray_size',
            //'handle_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
