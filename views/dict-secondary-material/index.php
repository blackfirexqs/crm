<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\dict\SecondaryMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Secondary Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secondary-material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dict-button-group">
        <?= Html::a('Создать новую запись', ['create'], ['class' => 'btn btn-success']) ?>

        <?php $upd_form = ActiveForm::begin(); ?>
        <?= Html::submitButton('Добавить записи из файла', ['class' => 'btn btn-primary', 'onclick' => 'return confirm(\'Вы действительно хотите добавить новые записи из файла?\')']) ?>
        <?php echo $upd_form->field($update_form, 'update_button')->label(false)->hiddenInput(['value' => 'update_button']); ?>
        <?php ActiveForm::end(); ?>

        <p class="red-info">Добавление элементов осуществляется из файла находящего в "..files/dict/secondary_material.xlsx" </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'secondary_material_id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
