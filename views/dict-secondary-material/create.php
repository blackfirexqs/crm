<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\SecondaryMaterial */

$this->title = 'Create Secondary Material';
$this->params['breadcrumbs'][] = ['label' => 'Secondary Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secondary-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
