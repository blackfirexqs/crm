<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\dict\OrderType */

$this->title = 'Update Order Type: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Order Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->order_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
